import asyncio
from unittest.mock import AsyncMock, Mock

import pytest

from allsky_tmp_control.application import Application
from allsky_tmp_control.cpu_tmp_sensor.cpu_sensor import CpuSensor
from allsky_tmp_control.header_device.sensor.dummy_sensor import DummySensor
from allsky_tmp_control.regulator import CoolingRegulator


def test_sleep_time():
    app = Application(10.0, None, None, [], None)

    assert app._sleep_time == 1/10.0


@pytest.mark.asyncio
async def test_start():
    app = Application(1.0, None, None, [], None)

    app._setup = AsyncMock()
    app._run = AsyncMock()

    await app.start()

    app._setup.assert_called_once()
    app._run.assert_called_once()


@pytest.mark.asyncio
async def test_setup():
    cpu_sensor = CpuSensor(["echo", "44.1"], r"\d*.\d*")

    sensor = DummySensor(1, None)
    sensor.setup = AsyncMock(return_value=10.0)

    regulator = CoolingRegulator(None, 1, 1, 90.0)
    regulator.setup = AsyncMock()

    app = Application(1.0, cpu_sensor, sensor, [regulator], None)
    await app._setup()

    sensor.setup.assert_called_once()
    regulator.setup.assert_called_once()


@pytest.mark.asyncio
async def test_loop():
    asyncio.sleep = AsyncMock()

    cpu_sensor = CpuSensor([], r"")
    cpu_sensor.measure = AsyncMock(return_value=30.0)

    measurement_log = Mock()

    sensor = DummySensor(1, None)
    sensor.measure = AsyncMock(return_value={"temperature": 10.0})

    regulator = Mock()

    app = Application(1.0, cpu_sensor, sensor, [regulator], measurement_log)

    await app._loop()

    sensor.measure.assert_called_once()
    regulator.assert_called_once_with(30.0, 10.0)
    asyncio.sleep.assert_called_once_with(1.0)

    measurement_log.assert_called_once_with({"temperature": 10.0})
