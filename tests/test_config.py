from unittest.mock import Mock

from allsky_tmp_control.config import Config


def test_load():
    config = Config("filepath")

    config_dict = {
        "update_cadence": 1.0,
        "cpu_sensor":
            {
                "command": ["echo", "44.1"],
                "pattern": r"\d*.\d*"
            },
        "sensor": {
            "type_name": "DummySensor",
            "pin": 1
        },
        "regulators":
            [
                {
                    "type_name": "DummyRegulator",
                    "target_temp": 10.0,
                    "tolerance": 5.0,
                    "cpu_max": 90.0,
                    "relay":
                        {
                            "type_name": "DummyRelay",
                            "pin": 2
                        }
                }
            ],
        "measurement_log": {"type_name": "local", "options": {"file_path": "/tmp/measurements.log"}},
        "testing": True
    }

    config._load_file = Mock(return_value=config_dict)
    config.load()
    
    assert config.update_cadence == 1.0
    assert config.sensor.type_name == "DummySensor"
    assert config.sensor.pin == 1

    assert config.regulators[0].type_name == "DummyRegulator"
    assert config.regulators[0].target_temp == 10.0
    assert config.regulators[0].tolerance == 5.0
    assert config.regulators[0].cpu_max == 90.0
    assert config.regulators[0].relay_config.type_name == "DummyRelay"
    assert config.regulators[0].relay_config.pin == 2

    assert config.cpu_sensor.command == ["echo", "44.1"]
    assert config.cpu_sensor.pattern == r"\d*.\d*"

    assert config.measurement_log.type_name == "local"
    assert config.measurement_log.options["file_path"] == "/tmp/measurements.log"

    assert config.testing == True
