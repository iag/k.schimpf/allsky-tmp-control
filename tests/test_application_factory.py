from unittest.mock import Mock

from allsky_tmp_control.application_factory import ApplicationFactory
from allsky_tmp_control.config import Config, CpuSensorConfig, DeviceConfig, RegulatorConfig, MeasurementLogConfig
from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.sensor.dummy_sensor import DummySensor
from allsky_tmp_control.measurement_log.local import LocalMeasurementLog
from allsky_tmp_control.measurement_log.measurement_log_factory import MeasurementLogFactory
from allsky_tmp_control.regulator import HeatingRegulator


def test_build():
    pin_manger = DummyPinManager()
    application_factory = ApplicationFactory(pin_manger)

    sensor = DummySensor(1, None)
    application_factory._sensor_factory.create = Mock(return_value=sensor)
    regulator = HeatingRegulator(None, 1, 1.0, 90.0)
    application_factory._regulator_factory.create = Mock(return_value=regulator)

    measurement_log = LocalMeasurementLog("")
    MeasurementLogFactory.create = Mock(return_value=measurement_log)

    config = Config("")
    config.update_cadence = 1/60
    config.cpu_sensor = CpuSensorConfig([], "")
    config.sensor = DeviceConfig("DummySensor", 1)
    config.regulators = [RegulatorConfig("HeatingRegulator", 1.0, 1.0, 90.0, None)]
    config.measurement_log = MeasurementLogConfig("DummyMeasurementLog", {})

    application = application_factory.build(config)

    assert application._sleep_time == 60
    assert application._cpu_sensor._command == []

    assert application._sensor == sensor
    assert application._regulators[0] == regulator
    assert application._measurement_log == measurement_log

