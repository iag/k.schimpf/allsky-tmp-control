from unittest.mock import Mock

import pytest

from allsky_tmp_control.config import RegulatorConfig, DeviceConfig
from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.relay.dummy_relay import DummyRelay
from allsky_tmp_control.regulator import CoolingRegulator, HeatingRegulator
from allsky_tmp_control.regulator.regulator_factory import RegulatorFactory


@pytest.fixture()
def regulator_factory() -> RegulatorFactory:
    pin_manager = DummyPinManager()
    regulator_factory = RegulatorFactory(pin_manager)
    relay = DummyRelay(1, None)

    regulator_factory._relay_factory.create = Mock(return_value=relay)
    return regulator_factory


def test_create_relay(regulator_factory):

    relay = DummyRelay(1, None)
    regulator_factory._relay_factory.create = Mock(return_value=relay)

    relay_config = DeviceConfig(type_name="DummyRelay", pin=1)
    config = RegulatorConfig("CoolingRegulator", 10.0, 5.0, 90.0,
                             relay_config)
    regulator_factory.create(config)

    regulator_factory._relay_factory.create.assert_called_once_with(relay_config)


def test_create_cooling_regulator(regulator_factory):
    config = RegulatorConfig("CoolingRegulator", 10.0, 5.0, 90.0,
                             None)
    regulator = regulator_factory.create(config)

    assert isinstance(regulator, CoolingRegulator)
    assert regulator._target_temp == 10.0
    assert regulator._tolerance == 5.0
    assert regulator._cpu_max == 90.0


def test_create_heating_regulator(regulator_factory):
    config = RegulatorConfig("HeatingRegulator", 10.0, 5.0, 90.0,
                             None)
    regulator = regulator_factory.create(config)

    assert isinstance(regulator, HeatingRegulator)
    assert regulator._target_temp == 10.0
    assert regulator._tolerance == 5.0
    assert regulator._cpu_max == 90.0


def test_create_invalid(regulator_factory):
    config = RegulatorConfig("Nonsense", 10.0, 5.0, 90.0,
                             None)

    with pytest.raises(TypeError):
        regulator_factory.create(config)

