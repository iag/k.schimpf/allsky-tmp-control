from unittest.mock import Mock

from allsky_tmp_control.regulator import HeatingRegulator
from allsky_tmp_control.header_device.relay.dummy_relay import DummyRelay


def test_low():
    relay = DummyRelay(None, None)
    relay.turn_on = Mock()
    regulator = HeatingRegulator(relay, 10.0, 5.0, 90.0)

    regulator(20.0, 0.0)

    relay.turn_on.assert_called_once()


def test_high():
    relay = DummyRelay(None, None)
    relay.turn_off = Mock()
    regulator = HeatingRegulator(relay, 10.0, 5.0, 90.0)

    regulator(20.0, 20.0)

    relay.turn_off.assert_called_once()


def test_cpu_high():
    relay = DummyRelay(None, None)
    relay.turn_off = Mock()
    regulator = HeatingRegulator(relay, 10.0, 5.0, 90.0)

    regulator(91.0, 10.0)

    relay.turn_off.assert_called_once()
