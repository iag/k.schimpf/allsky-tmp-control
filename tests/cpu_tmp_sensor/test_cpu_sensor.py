from unittest.mock import Mock

import pytest

from allsky_tmp_control.cpu_tmp_sensor.cpu_sensor import CpuSensor


@pytest.mark.asyncio
async def test_measure():
    sensor = CpuSensor([""], pattern=r"temp=(\d*.\d*)'C")
    sensor._measure = Mock(return_value="temp=44.8'C")

    assert await sensor.measure() == 44.8


@pytest.mark.asyncio
async def test_invalid_measurement_text():
    sensor = CpuSensor([""], r"")
    sensor._measure = Mock(return_value="temp=44,8'C")

    assert await sensor.measure() == 100.0
