from unittest.mock import Mock

import pytest

from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.relay.rpi_relay import RPIRelay


@pytest.mark.asyncio
async def test_setup():
    pin_manager = DummyPinManager()
    pin_manager.setup = Mock()
    pin_manager.setup_pin = Mock()

    relay = RPIRelay(1, pin_manager=pin_manager)
    await relay.setup()

    pin_manager.setup.assert_called_once()
    pin_manager.setup_pin.assert_called_once_with(1, as_input=False)


def test_turn_on():
    pin_manager = DummyPinManager()
    pin_manager.output_pin = Mock()

    relay = RPIRelay(1, pin_manager=pin_manager)
    relay.turn_on()

    pin_manager.output_pin.assert_called_once_with(1, state=False)


def test_turn_off():
    pin_manager = DummyPinManager()
    pin_manager.output_pin = Mock()

    relay = RPIRelay(1, pin_manager=pin_manager)
    relay.turn_off()

    pin_manager.output_pin.assert_called_once_with(1, state=True)
