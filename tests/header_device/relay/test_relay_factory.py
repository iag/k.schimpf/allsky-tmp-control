import pytest

from allsky_tmp_control.config import DeviceConfig
from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.relay.dummy_relay import DummyRelay
from allsky_tmp_control.header_device.relay.relay_factory import RelayFactory
from allsky_tmp_control.header_device.relay.rpi_relay import RPIRelay


def test_rpi_relay():
    pin_manager = DummyPinManager()
    relay_factory = RelayFactory(pin_manager)
    config = DeviceConfig("RPIRelay", 1)

    relay = relay_factory.create(config)

    assert isinstance(relay, RPIRelay)
    assert relay._pin == 1
    assert relay._pin_manager == pin_manager


def test_dummy_relay():
    pin_manager = DummyPinManager()
    relay_factory = RelayFactory(pin_manager)
    config = DeviceConfig("DummyRelay", 1)

    relay = relay_factory.create(config)

    assert isinstance(relay, DummyRelay)
    assert relay._pin == 1
    assert relay._pin_manager == pin_manager


def test_invalid():
    pin_manager = DummyPinManager()
    relay_factory = RelayFactory(pin_manager)
    config = DeviceConfig("Relay", 1)

    with pytest.raises(TypeError):
        relay_factory.create(config)