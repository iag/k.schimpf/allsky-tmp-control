import logging

import pytest

from allsky_tmp_control.header_device.relay.dummy_relay import DummyRelay


@pytest.mark.asyncio
async def test_setup(caplog) -> None:
    relay = DummyRelay(0, None)

    with caplog.at_level(logging.INFO):
        await relay.setup()

    assert caplog.messages[0] == "Setup Dummy Relay"


def test_turn_on(caplog) -> None:
    relay = DummyRelay(0, None)

    with caplog.at_level(logging.INFO):
        relay.turn_on()

    assert caplog.messages[0] == "Turned on Dummy Relay"


def test_turn_off(caplog) -> None:
    relay = DummyRelay(0, None)

    with caplog.at_level(logging.INFO):
        relay.turn_off()

    assert caplog.messages[0] == "Turned off Dummy Relay"
