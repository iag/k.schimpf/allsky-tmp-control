import logging

import pytest

from allsky_tmp_control.header_device.sensor.dummy_sensor import DummySensor


@pytest.mark.asyncio
async def test_setup(caplog):
    sensor = DummySensor(1, None)
    with caplog.at_level(logging.INFO):
        await sensor.setup()

    assert caplog.messages[0] == "Setup Dummy Sensor"


@pytest.mark.asyncio
async def test_measure():
    sensor = DummySensor(1, None)
    result = await sensor.measure()
    assert result["temperature"] == 10.0
