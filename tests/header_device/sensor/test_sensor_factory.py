import pytest

from allsky_tmp_control.config import DeviceConfig
from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.sensor.dht22_sensor import DHT22Sensor
from allsky_tmp_control.header_device.sensor.dummy_sensor import DummySensor
from allsky_tmp_control.header_device.sensor.linkerkit_sensor import LinkerKitSensor
from allsky_tmp_control.header_device.sensor.sensor_factory import SensorFactory


def test_linkerkit_sensor():
    pin_manager = DummyPinManager()
    relay_factory = SensorFactory(pin_manager)
    config = DeviceConfig("LinkerKitSensor", 1)

    relay = relay_factory.create(config)

    assert isinstance(relay, LinkerKitSensor)
    assert relay._pin == 1
    assert relay._pin_manager == pin_manager


def test_dht22_sensor():
    pin_manager = DummyPinManager()
    relay_factory = SensorFactory(pin_manager)
    config = DeviceConfig("DHT22Sensor", 4)

    relay = relay_factory.create(config)

    assert isinstance(relay, DHT22Sensor)
    assert relay._pin == 4
    assert relay._pin_manager == pin_manager


def test_dummy_sensor():
    pin_manager = DummyPinManager()
    relay_factory = SensorFactory(pin_manager)
    config = DeviceConfig("DummySensor", 1)

    relay = relay_factory.create(config)

    assert isinstance(relay, DummySensor)
    assert relay._pin == 1
    assert relay._pin_manager == pin_manager


def test_invalid():
    pin_manager = DummyPinManager()
    relay_factory = SensorFactory(pin_manager)
    config = DeviceConfig("Relay", 1)

    with pytest.raises(TypeError):
        relay_factory.create(config)
