from allsky_tmp_control.config import Config
from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.pin_manager.pin_manager_factory import PinManagerFactory
from allsky_tmp_control.header_device.pin_manager.rpi_pin_manger import RPIPinManager


def test_create_dummy():
    config = Config("")
    config.testing = True

    pin_manager = PinManagerFactory.create(config)
    assert isinstance(pin_manager, DummyPinManager)


def test_create_real():
    config = Config("")
    config.testing = False

    pin_manager = PinManagerFactory.create(config)
    assert isinstance(pin_manager, RPIPinManager)
