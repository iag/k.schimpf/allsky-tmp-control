import logging

from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager


def test_setup(caplog) -> None:
    relay = DummyPinManager()

    with caplog.at_level(logging.INFO):
        relay.setup()

    assert caplog.messages[0] == "Dummy pin manager setup"


def test_setup_pin(caplog) -> None:
    relay = DummyPinManager()

    with caplog.at_level(logging.INFO):
        relay.setup_pin(1, True)
        relay.setup_pin(1, False)

    assert caplog.messages[0] == "Setup pin 1 as input"
    assert caplog.messages[1] == "Setup pin 1 as output"


def test_output_pin(caplog) -> None:
    relay = DummyPinManager()

    with caplog.at_level(logging.INFO):
        relay.output_pin(1, True)
        relay.output_pin(1, False)

    assert caplog.messages[0] == "Set pin 1 state high"
    assert caplog.messages[1] == "Set pin 1 state low"


def test_input_pin(caplog) -> None:
    relay = DummyPinManager()

    assert relay.input_pin(1)


def test_teardown(caplog) -> None:
    relay = DummyPinManager()

    with caplog.at_level(logging.INFO):
        relay.teardown()

    assert caplog.messages[0] == "Teardown pin manager"