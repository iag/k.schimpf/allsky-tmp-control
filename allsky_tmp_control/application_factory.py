from allsky_tmp_control.application import Application
from allsky_tmp_control.config import Config
from allsky_tmp_control.cpu_tmp_sensor.cpu_sensor import CpuSensor
from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.sensor.sensor_factory import SensorFactory
from allsky_tmp_control.measurement_log.measurement_log_factory import MeasurementLogFactory
from allsky_tmp_control.regulator.regulator_factory import RegulatorFactory


class ApplicationFactory(object):
    def __init__(self, pin_manager: PinManager) -> None:
        self._sensor_factory = SensorFactory(pin_manager)
        self._regulator_factory = RegulatorFactory(pin_manager)

    def build(self, config: Config) -> Application:
        sensor = self._sensor_factory.create(config.sensor)
        cpu_sensor = CpuSensor(config.cpu_sensor.command, config.cpu_sensor.pattern)

        regulators = list(
            map(lambda x: self._regulator_factory.create(x),
                config.regulators
                )
        )

        measurement_log = MeasurementLogFactory.create(config.measurement_log)

        return Application(config.update_cadence, cpu_sensor, sensor, regulators, measurement_log)
