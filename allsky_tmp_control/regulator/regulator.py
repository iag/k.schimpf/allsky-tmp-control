import abc

from allsky_tmp_control.header_device.relay.relay import Relay


class Regulator(object, metaclass=abc.ABCMeta):
    def __init__(self, relay: Relay):
        self._relay = relay

    @abc.abstractmethod
    def __call__(self, cpu_temperature: float, temperature: float) -> None:
        ...

    async def setup(self):
        await self._relay.setup()
