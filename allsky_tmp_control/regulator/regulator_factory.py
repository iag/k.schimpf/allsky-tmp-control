from allsky_tmp_control.config import RegulatorConfig
from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.relay.relay_factory import RelayFactory
from allsky_tmp_control.regulator import CoolingRegulator, HeatingRegulator


class RegulatorFactory(object):
    def __init__(self, pin_manager: PinManager):
        self._relay_factory = RelayFactory(pin_manager)

    def create(self, config: RegulatorConfig):
        relay = self._relay_factory.create(config.relay_config)

        if config.type_name == "CoolingRegulator":
            return CoolingRegulator(relay, config.target_temp, config.tolerance, config.cpu_max)
        elif config.type_name == "HeatingRegulator":
            return HeatingRegulator(relay, config.target_temp, config.tolerance, config.cpu_max)
        else:
            raise TypeError("Unknown regulator type")
