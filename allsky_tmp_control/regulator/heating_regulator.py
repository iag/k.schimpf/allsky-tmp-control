from allsky_tmp_control.header_device.relay.relay import Relay
from allsky_tmp_control.regulator.regulator import Regulator


class HeatingRegulator(Regulator):
    def __init__(self, relay: Relay, target_temp: float, tolerance: float, cpu_max: float) -> None:
        super().__init__(relay)
        self._target_temp = target_temp
        self._tolerance = tolerance
        self._cpu_max = cpu_max

    def __call__(self, cpu_temperature: float, temperature: float) -> None:
        if cpu_temperature > self._cpu_max or temperature > self._target_temp + self._tolerance:
            self._relay.turn_off()

        elif temperature < self._target_temp - self._tolerance:
            self._relay.turn_on()
