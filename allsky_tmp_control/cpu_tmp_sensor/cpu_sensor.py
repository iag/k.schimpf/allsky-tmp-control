import re
import subprocess
from typing import List


class CpuSensor(object):
    def __init__(self, command: List[str], pattern: str):
        if command is None:
            command = ["vcgencmd", "measure_temp"]

        self._command: List[str] = command

        self._measurement_regex = re.compile(pattern)

    async def measure(self) -> float:
        measurement_text = self._measure()

        try:
            return self._parse_measurement(measurement_text)
        except Exception:
            return 100.0

    def _measure(self) -> str:
        result = subprocess.run(self._command, stdout=subprocess.PIPE)
        return result.stdout.decode("utf-8")

    def _parse_measurement(self, measurement_text: str) -> float:
        match = self._measurement_regex.match(measurement_text)

        if match is None or len(match.groups()) != 1:
            raise Exception("Could not read raspberry cpu temperature!")

        measurement = float(match.groups()[0])
        return measurement
