from typing import Dict

import influxdb_client
from influxdb_client.client.write_api import SYNCHRONOUS
from datetime import datetime


class InfluxMeasurementLog:
    def __init__(self, url: str, bucket: str, org: str, token: str) -> None:
        self._client = influxdb_client.InfluxDBClient(
            url=url,
            token=token,
            org=org
        )
        self._bucket = bucket
        self._org = org

    def __call__(self, measurements: Dict[str, float]):
        data = influxdb_client.Point("measurement")

        data.time(datetime.now())

        for key, value in measurements.items():
            data.field(key, value)

        with self._client.write_api(write_options=SYNCHRONOUS) as write_api:
            write_api.write(bucket=self._bucket, org=self._org, record=data)
