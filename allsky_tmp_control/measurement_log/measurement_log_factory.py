from allsky_tmp_control.config import MeasurementLogConfig
from allsky_tmp_control.measurement_log.influx import InfluxMeasurementLog
from allsky_tmp_control.measurement_log.local import LocalMeasurementLog


class MeasurementLogFactory:
    @staticmethod
    def create(config: MeasurementLogConfig):
        if config.type_name == "Influx":
            return InfluxMeasurementLog(**config.options)
        elif config.type_name == "Local":
            return LocalMeasurementLog(**config.options)
        else:
            raise TypeError("Unknown measurement log type")
