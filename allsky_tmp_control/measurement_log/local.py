from typing import Dict


class LocalMeasurementLog:
    def __init__(self, file_path: str, mode: str = "w ") -> None:
        self._file_path = file_path
        self._mode = mode

    def __call__(self, measurements: Dict[str, float]):
        text = ", ".join(map(lambda x: f"{x[0]}={x[1]}", measurements.items())) + "\n"
        with open(self._file_path, self._mode) as file:
            file.write(text)
