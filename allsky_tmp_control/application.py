import asyncio
from typing import List, Callable, Dict

from allsky_tmp_control.cpu_tmp_sensor.cpu_sensor import CpuSensor
from allsky_tmp_control.regulator.regulator import Regulator
from allsky_tmp_control.header_device.sensor.sensor import Sensor


class Application(object):
    def __init__(self,
                 update_cadence: float,
                 cpu_sensor: CpuSensor,
                 sensor: Sensor,
                 regulators: List[Regulator],
                 measurement_log: Callable[[Dict[str, float]], None]):
        self._sleep_time = 1/update_cadence
        self._cpu_sensor = cpu_sensor
        self._sensor = sensor
        self._regulators = regulators
        self._measurement_log = measurement_log

    async def start(self) -> None:
        await self._setup()
        await self._run()

    async def _setup(self) -> None:
        await self._sensor.setup()

        for regulator in self._regulators:
            await regulator.setup()

    async def _run(self) -> None:
        while True:
            await self._loop()

    async def _loop(self) -> None:
        cpu_temperature = await self._cpu_sensor.measure()
        measurements = await self._sensor.measure()

        for regulator in self._regulators:
            regulator(cpu_temperature, measurements["temperature"])

        try:
            self._measurement_log(measurements)
        except Exception as e:
            print(e)

        await asyncio.sleep(self._sleep_time)
