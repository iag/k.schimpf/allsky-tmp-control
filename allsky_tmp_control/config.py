from typing import Optional, List, Dict, Any

import yaml


class CpuSensorConfig(object):
    def __init__(self, command: Optional[List[str]] = None, pattern: str = r"temp=(\d*.\d*)'C"):
        self.command = command
        self.pattern = pattern


class DeviceConfig(object):
    def __init__(self, type_name: str, pin: int):
        self.type_name = type_name
        self.pin = pin


class RegulatorConfig(object):
    def __init__(self, type_name: str, target_temp: float, tolerance: float, cpu_max: float, relay_config: DeviceConfig):
        self.type_name = type_name
        self.target_temp = target_temp
        self.tolerance = tolerance
        self.cpu_max = cpu_max
        self.relay_config = relay_config


class MeasurementLogConfig(object):
    def __init__(self, type_name: str, options: Dict[str, str]) -> None:
        self.type_name = type_name
        self.options = options


class Config(object):
    def __init__(self, file_path: str) -> None:
        self._file_path = file_path

        self.update_cadence: Optional[float] = None
        self.cpu_sensor: Optional[CpuSensorConfig] = None
        self.sensor: Optional[DeviceConfig] = None
        self.regulators: Optional[List[RegulatorConfig]] = None
        self.measurement_log: Optional[MeasurementLogConfig] = None
        self.testing: Optional[bool] = None

    def load(self) -> None:
        config = self._load_file()
        self.cpu_sensor = CpuSensorConfig(**config["cpu_sensor"])
        self.update_cadence = float(config["update_cadence"])
        self.sensor = DeviceConfig(**config["sensor"])
        self.regulators = list(
            map(lambda x: RegulatorConfig(x["type_name"], x["target_temp"], x["tolerance"], x["cpu_max"], DeviceConfig(**x["relay"])),
                config["regulators"]
                )
        )
        self.measurement_log = MeasurementLogConfig(
            config["measurement_log"]["type_name"],
            config["measurement_log"]["options"])

        self.testing = config.get("testing", False)

    def _load_file(self) -> Dict[str, Any]:
        with open(self._file_path, "r") as file:
            return yaml.safe_load(file)
