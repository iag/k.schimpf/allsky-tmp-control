import abc

from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager


class Device(object, metaclass=abc.ABCMeta):
    def __init__(self, pin: int, pin_manager: PinManager) -> None:
        self._pin = pin
        self._pin_manager = pin_manager

    @abc.abstractmethod
    async def setup(self) -> None:
        """
        Sets up the header_device
        """