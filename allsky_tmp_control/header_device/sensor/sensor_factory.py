from allsky_tmp_control.config import DeviceConfig
from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.sensor.dht22_sensor import DHT22Sensor
from allsky_tmp_control.header_device.sensor.dummy_sensor import DummySensor
from allsky_tmp_control.header_device.sensor.linkerkit_sensor import LinkerKitSensor


class SensorFactory(object):
    def __init__(self, pin_manager: PinManager):
        self._pin_manager = pin_manager

    def create(self, config: DeviceConfig):
        if config.type_name == "LinkerKitSensor":
            return LinkerKitSensor(config.pin, self._pin_manager)
        elif config.type_name == "DHT22Sensor":
            return DHT22Sensor(config.pin, self._pin_manager)
        elif config.type_name == "DummySensor":
            return DummySensor(config.pin, self._pin_manager)
        else:
            raise TypeError("Unknown sensor type")
