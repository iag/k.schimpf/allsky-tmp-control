import abc
from typing import Dict

from allsky_tmp_control.header_device.device import Device


class Sensor(Device, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    async def measure(self) -> Dict[str, "float"]:
        """
        Measures the sensor value
        :return: Current temperature in Celsius
        """
