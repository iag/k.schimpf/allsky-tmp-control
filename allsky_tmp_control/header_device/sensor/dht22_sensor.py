import asyncio
import logging
from typing import Dict

from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.sensor.sensor import Sensor


class DHT22Sensor(Sensor):
    def __init__(self, pin: int, pin_manager: PinManager):
        super().__init__(pin, pin_manager)
        self._sensor = None

    async def setup(self) -> None:
        from pigpio_dht import DHT22
        self._sensor = DHT22(self._pin, timeout_secs=2)

    async def measure(self) -> Dict[str, float]:
        measurement = None
        while measurement is None:
            reading = self._sensor.read(retries=5)
            logging.debug(f"Reading: {reading}")
            if reading["valid"]:
                measurement = {"temperature": reading["temp_c"], "humidity": reading["humidity"]}
            else:
                await asyncio.sleep(2)

        return measurement
