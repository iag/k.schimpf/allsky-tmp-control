import logging
from typing import Dict

from allsky_tmp_control.header_device.sensor.sensor import Sensor

logger = logging.getLogger(__name__)


class DummySensor(Sensor):
    async def setup(self) -> None:
        logger.info("Setup Dummy Sensor")

    async def measure(self) -> Dict[str, float]:
        return {"temperature": 10.0}

