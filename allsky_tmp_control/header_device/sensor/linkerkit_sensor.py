import asyncio
import glob
import os
from typing import Optional, AnyStr, List, Dict

from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.sensor.sensor import Sensor


class LinkerKitSensor(Sensor):

    DEVICE_BASE_DIR = "/sys/bus/w1/devices/"
    DEVICE_FILE_NAME = "w1_slave"

    def __init__(self, pin: int, pin_manager: PinManager) -> None:
        super().__init__(pin, pin_manager)

        self._device_file_path: Optional[str] = None

    async def setup(self) -> None:
        self._pin_manager.setup_pin(self._pin, as_input=True)
        await self._find_device_file()

        self._measure()    # Raw reading for initialization

    async def _find_device_file(self) -> None:
        device_folder = None

        while device_folder is None:
            device_folder = await self._find_device_folder()

        self._device_file_path = os.path.join(self.DEVICE_BASE_DIR, device_folder, self.DEVICE_FILE_NAME)

    async def _find_device_folder(self) -> Optional[str]:
        try:
            device_folder = glob.glob(self.DEVICE_BASE_DIR + '28*')[0]
            return device_folder
        except IndexError:
            await asyncio.sleep(0.5)

        return None

    def _measure(self) -> List[AnyStr]:
        """
        Reading triggers measurement
        :return: Raw measurement data
        """
        with open(self._device_file_path, "r") as file:
            return file.readlines()

    async def measure(self) -> Dict[str, float]:
        temp_measurement = None
        
        while temp_measurement is None:
            reading = await self._get_valid_measurement()
            temp_measurement = self._extract_temp_from_reading(reading)

        return {"temperature": temp_measurement}
            
    async def _get_valid_measurement(self) -> List[AnyStr]:
        reading = self._measure()
        while not self._is_valid_reading(reading):
            await asyncio.sleep(0.2)
            reading = self._measure()

        return reading

    @staticmethod
    def _is_valid_reading(lines) -> bool:
        return lines[0].strip()[-3:] == 'YES'

    @staticmethod
    def _extract_temp_from_reading(reading: List[AnyStr]) -> Optional[float]:
        equals_pos = reading[1].find('t=')
        if equals_pos != -1:
            temp_string = reading[1][equals_pos + 2:]
            return float(temp_string) / 1000.0

        return None

