import abc


class PinManager(object, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def setup(self):
        ...

    @abc.abstractmethod
    def setup_pin(self, pin: int, as_input: bool) -> None:
        ...

    @abc.abstractmethod
    def output_pin(self, pin: int, state: bool) -> None:
        ...

    @abc.abstractmethod
    def input_pin(self, pin: int) -> bool:
        ...

    @abc.abstractmethod
    def teardown(self) -> None:
        ...