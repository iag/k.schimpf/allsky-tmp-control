import logging

from typing import Set, Dict

from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager

logger = logging.getLogger(__name__)


class RPIPinManager(PinManager):
    def __init__(self):
        self._is_setup = False
        self._pins: Dict[bool, Set[int]] = {True: set(), False: set()}

    def setup(self):
        import RPi.GPIO as GPIO
        if not self._is_setup:
            GPIO.setmode(GPIO.BCM)

    def setup_pin(self, pin: int, as_input: bool) -> None:
        self.setup()
        self._add_pin(pin, as_input)
        self._init_pin(pin, as_input)

        logger.debug(f"Setup pin {pin} as {'input' if as_input else 'output'}")

    def _add_pin(self, pin: int, as_input: bool) -> None:
        if pin in self._pins[as_input]:
            raise Exception("Pin already used!")

        self._pins[as_input].add(pin)

    def _init_pin(self, pin, as_input: bool) -> None:
        import RPi.GPIO as GPIO
        if as_input:
            GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        else:
            GPIO.setup(pin, GPIO.OUT, initial=True)

    def output_pin(self, pin: int, state: bool) -> None:
        import RPi.GPIO as GPIO
        self._check_input_mode(pin, False)
        GPIO.output(pin, state)

        logger.info(f"Set pin {pin} state {'high' if state else 'low'}")

    def input_pin(self, pin: int) -> bool:
        import RPi.GPIO as GPIO
        self._check_input_mode(pin, False)
        return GPIO.input(pin)

    def _check_input_mode(self, pin: int, as_input: bool) -> None:
        if pin not in self._pins[as_input]:
            raise Exception("Pin is not setup correctly!")

    def teardown(self) -> None:
        import RPi.GPIO as GPIO
        GPIO.cleanup()
