import logging

from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager

logger = logging.getLogger(__name__)


class DummyPinManager(PinManager):
    def setup(self):
        logger.info("Dummy pin manager setup")

    def setup_pin(self, pin: int, as_input: bool) -> None:
        logger.info(f"Setup pin {pin} as {'input' if as_input else 'output'}")

    def output_pin(self, pin: int, state: bool) -> None:
        logger.info(f"Set pin {pin} state {'high' if state else 'low'}")

    def input_pin(self, pin: int) -> bool:
        return True

    def teardown(self) -> None:
        logger.info("Teardown pin manager")
