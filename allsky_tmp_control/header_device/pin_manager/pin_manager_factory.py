from allsky_tmp_control.config import Config
from allsky_tmp_control.header_device.pin_manager.dummy_pin_manager import DummyPinManager
from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.pin_manager.rpi_pin_manger import RPIPinManager


class PinManagerFactory:
    @staticmethod
    def create(config: Config) -> PinManager:
        if config.testing:
            return DummyPinManager()
        else:
            return RPIPinManager()
