from allsky_tmp_control.header_device.relay.relay import Relay


class RPIRelay(Relay):
    async def setup(self) -> None:
        self._pin_manager.setup()
        self._pin_manager.setup_pin(self._pin, as_input=False)

    def turn_on(self):
        self._pin_manager.output_pin(self._pin, state=False)

    def turn_off(self):
        self._pin_manager.output_pin(self._pin, state=True)
