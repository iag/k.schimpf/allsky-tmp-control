import abc

from allsky_tmp_control.header_device.device import Device


class Relay(Device, metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def turn_on(self):
        ...

    @abc.abstractmethod
    def turn_off(self):
        ...