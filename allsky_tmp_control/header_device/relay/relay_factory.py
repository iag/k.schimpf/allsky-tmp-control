from allsky_tmp_control.config import DeviceConfig
from allsky_tmp_control.header_device.pin_manager.pin_manager import PinManager
from allsky_tmp_control.header_device.relay.dummy_relay import DummyRelay
from allsky_tmp_control.header_device.relay.relay import Relay
from allsky_tmp_control.header_device.relay.rpi_relay import RPIRelay


class RelayFactory(object):
    def __init__(self, pin_manager: PinManager):
        self._pin_manager = pin_manager

    def create(self, config: DeviceConfig) -> Relay:
        if config.type_name == "RPIRelay":
            return RPIRelay(config.pin, self._pin_manager)
        elif config.type_name == "DummyRelay":
            return DummyRelay(config.pin, self._pin_manager)
        else:
            raise TypeError("Unknown relay type!")
