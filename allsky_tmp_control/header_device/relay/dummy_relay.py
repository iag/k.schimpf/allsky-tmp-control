import logging

from allsky_tmp_control.header_device.relay.relay import Relay

logger = logging.getLogger(__name__)


class DummyRelay(Relay):
    async def setup(self) -> None:
        logger.info("Setup Dummy Relay")

    def turn_on(self):
        logger.info("Turned on Dummy Relay")

    def turn_off(self):
        logger.info("Turned off Dummy Relay")
