# Allsky Temperature Control
[![pipeline status](https://gitlab.gwdg.de/iag/k.schimpf/allsky-tmp-control/badges/main/pipeline.svg)](https://gitlab.gwdg.de/iag/k.schimpf/allsky-tmp-control/-/commits/main)
[![coverage report](https://gitlab.gwdg.de/iag/k.schimpf/allsky-tmp-control/badges/main/coverage.svg)](https://gitlab.gwdg.de/iag/k.schimpf/allsky-tmp-control/-/commits/main)
[![Latest Release](https://gitlab.gwdg.de/iag/k.schimpf/allsky-tmp-control/-/badges/release.svg)](https://gitlab.gwdg.de/iag/k.schimpf/allsky-tmp-control/-/releases) 
