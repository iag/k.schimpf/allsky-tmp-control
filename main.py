import asyncio
import logging
import sys

from allsky_tmp_control.application_factory import ApplicationFactory
from allsky_tmp_control.config import Config
from allsky_tmp_control.header_device.pin_manager.pin_manager_factory import PinManagerFactory


def main(config_file_path: str):
    init_logging()

    config = Config(config_file_path)
    config.load()

    pin_manager = PinManagerFactory.create(config)

    application_factory = ApplicationFactory(pin_manager)
    application = application_factory.build(config)

    try:
        asyncio.run(application.start())
    except KeyboardInterrupt:
        pin_manager.teardown()


def init_logging():
    root = logging.getLogger()
    root.setLevel(logging.DEBUG)

    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    root.addHandler(handler)


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python main.py [config filepath].")

    config_file_path = sys.argv[1]
    main(config_file_path)
